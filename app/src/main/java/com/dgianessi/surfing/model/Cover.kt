package com.dgianessi.surfing.model

enum class Cover {
    ROME,
    PARIS,
    NEW_YORK
}