package com.dgianessi.surfing.model

enum class Weather {
    SUNNY,
    CLOUDY
}