package com.dgianessi.surfing.model

import com.dgianessi.surfing.model.Weather.CLOUDY
import com.dgianessi.surfing.model.Weather.SUNNY

private const val WEATHER_TEMP_LIMIT = 30

data class City(val id: String, val name: String, val temperature: Int, val cover: Cover) {

    val weather: Weather = if (temperature < WEATHER_TEMP_LIMIT) CLOUDY else SUNNY

}

