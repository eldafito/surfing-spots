package com.dgianessi.surfing.shared

import com.dgianessi.surfing.network.MockyApi
import com.dgianessi.surfing.network.NumbersApi
import com.dgianessi.surfing.repository.CityRepository
import com.dgianessi.surfing.repository.CityRepositoryImpl
import com.dgianessi.surfing.repository.TemperatureRepository
import com.dgianessi.surfing.repository.TemperatureRepositoryImpl
import com.dgianessi.surfing.viewmodel.CitiesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { NumbersApi.create() }
    single { MockyApi.create() }

    single<CityRepository> { CityRepositoryImpl(get()) }
    single<TemperatureRepository> { TemperatureRepositoryImpl(get()) }

    viewModel {
        CitiesViewModel(
            cityRepository = get(),
            temperatureRepository = get()
        )
    }

}