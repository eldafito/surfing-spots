package com.dgianessi.surfing.shared

import android.app.Application
import org.koin.core.context.startKoin

class SurfingSpots : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(appModule)
        }
    }

}