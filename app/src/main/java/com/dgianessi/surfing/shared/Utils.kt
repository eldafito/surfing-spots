package com.dgianessi.surfing.shared

import com.dgianessi.surfing.model.City

fun Collection<City>.sortedByTemperature(): List<City> = sortedByDescending { c -> c.temperature }