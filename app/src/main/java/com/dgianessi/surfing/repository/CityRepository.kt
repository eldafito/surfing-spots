package com.dgianessi.surfing.repository

import com.dgianessi.surfing.network.MockyApi

interface CityRepository {

    suspend fun fetchNames(): List<String>

}

class CityRepositoryImpl(private val mockyApi: MockyApi) : CityRepository {

    override suspend fun fetchNames() =
        mockyApi.getCities().cities.map { it.name }

}