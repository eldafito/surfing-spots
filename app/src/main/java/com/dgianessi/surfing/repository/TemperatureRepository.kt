package com.dgianessi.surfing.repository

import com.dgianessi.surfing.network.NumbersApi

interface TemperatureRepository {

    suspend fun fetchRandomValue(): Int

}

class TemperatureRepositoryImpl(private val numbersApi: NumbersApi) : TemperatureRepository {

    override suspend fun fetchRandomValue(): Int =
        numbersApi.getRandom().takeWhile { it.isDigit() }.toInt()

}