package com.dgianessi.surfing.network

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MockyApi {

    companion object {

        private val BASE_URL = "https://run.mocky.io/v3/".toHttpUrl()

        fun create(httpUrl: HttpUrl = BASE_URL): MockyApi {
            return Retrofit.Builder()
                .baseUrl(httpUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MockyApi::class.java)
        }

    }

    @GET("652ceb94-b24e-432b-b6c5-8a54bc1226b6")
    suspend fun getCities(): CitiesRemote

}