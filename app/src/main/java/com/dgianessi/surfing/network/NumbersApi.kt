package com.dgianessi.surfing.network

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET

interface NumbersApi {

    companion object {

        private val BASE_URL = "http://numbersapi.com".toHttpUrl()

        fun create(httpUrl: HttpUrl = BASE_URL): NumbersApi {
            return Retrofit.Builder()
                .baseUrl(httpUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create(NumbersApi::class.java)
        }

    }

    @GET("random/math")
    suspend fun getRandom(): String

}