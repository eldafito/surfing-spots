package com.dgianessi.surfing.network

data class CitiesRemote(
    val cities: List<CityRemote>
) {
    data class CityRemote(
        val name: String
    )
}

