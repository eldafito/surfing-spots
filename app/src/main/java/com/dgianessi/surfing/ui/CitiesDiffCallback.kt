package com.dgianessi.surfing.ui

import androidx.recyclerview.widget.DiffUtil
import com.dgianessi.surfing.model.City

class CitiesDiffCallback : DiffUtil.ItemCallback<City>() {

    override fun areItemsTheSame(oldItem: City, newItem: City) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: City, newItem: City) = oldItem == newItem

}