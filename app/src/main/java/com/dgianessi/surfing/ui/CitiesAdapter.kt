package com.dgianessi.surfing.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dgianessi.surfing.R
import com.dgianessi.surfing.model.City
import com.dgianessi.surfing.model.Cover
import com.dgianessi.surfing.model.Cover.*
import com.dgianessi.surfing.model.Weather
import com.dgianessi.surfing.model.Weather.CLOUDY
import com.dgianessi.surfing.model.Weather.SUNNY

class CitiesAdapter : ListAdapter<City, CitiesAdapter.ViewHolder>(CitiesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.item_city, parent, false)
            )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val backImg = itemView.findViewById<ImageView>(R.id.city_back_img)
        private val titeTxt = itemView.findViewById<TextView>(R.id.city_title_txt)
        private val subheadTxt = itemView.findViewById<TextView>(R.id.city_subhead_txt)

        fun bind(city: City) {
            val context = itemView.context
            city.apply {
                backImg.setImageResource(cover.drawableResource)
                backImg.isVisible = weather == SUNNY
                titeTxt.text = name
                subheadTxt.text = context.getString(R.string.city_sub, context.getString(weather.stringResource), temperature)
            }
        }
    }

}

val Weather.stringResource
    get() = when (this) {
        SUNNY -> R.string.sunny
        CLOUDY -> R.string.cloudy
    }

val Cover.drawableResource
    get() = when (this) {
        ROME -> R.drawable.rome
        PARIS -> R.drawable.paris
        NEW_YORK -> R.drawable.new_york
    }