package com.dgianessi.surfing.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.dgianessi.surfing.R
import com.dgianessi.surfing.databinding.ActivityCitiesBinding
import com.dgianessi.surfing.viewmodel.CitiesViewModel
import com.dgianessi.surfing.viewmodel.CitiesViewModel.State.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CitiesActivity : AppCompatActivity() {

    private val viewModel: CitiesViewModel by viewModel()
    private val adapter = CitiesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityCitiesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.citiesRecycler.also { r ->
            r.adapter = adapter
            r.addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelSize(R.dimen.spacing_l)))
            r.setHasFixedSize(true)
        }

        viewModel.state.observe(this) { state ->
            val newCities = (state as? Success)?.cities.orEmpty()
            adapter.submitList(newCities)

            binding.citiesErrorMsg.isVisible = state is Failure
            binding.citiesProgress.isVisible = state == Loading
        }
    }


}