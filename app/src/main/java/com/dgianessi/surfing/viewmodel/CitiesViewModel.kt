package com.dgianessi.surfing.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.dgianessi.surfing.model.City
import com.dgianessi.surfing.model.Cover
import com.dgianessi.surfing.repository.CityRepository
import com.dgianessi.surfing.repository.TemperatureRepository
import com.dgianessi.surfing.shared.sortedByTemperature
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.*
import java.util.*

private val TEMP_RANGE = 0..100
private const val RETRIES = 1L
private const val UPDATE_INTERVAL = 3_000L

class CitiesViewModel(
    cityRepository: CityRepository,
    temperatureRepository: TemperatureRepository
) : ViewModel() {

    companion object {
        const val TAG = "CitiesViewModel"
    }

    private val cities = flow {
        val names = cityRepository.fetchNames()
        val mutableCities = names.map { n ->
            City(
                id = UUID.randomUUID().toString(),
                name = n,
                temperature = TEMP_RANGE.random(),
                cover = Cover.values().random()
            )
        }.toMutableList()
        emit(mutableCities.sortedByTemperature())

        val ids = mutableCities.map { it.id }
        val remainingCitiesInRound = mutableListOf<String>()

        ticker(initialDelayMillis = UPDATE_INTERVAL, delayMillis = UPDATE_INTERVAL).consumeEach {
            if (remainingCitiesInRound.isEmpty()) {
                remainingCitiesInRound.addAll(ids.shuffled().toMutableList())
                Log.d(TAG, "Round: new round with $remainingCitiesInRound")
            }

            val randomCityId = remainingCitiesInRound.removeFirst()
            val randomCity = mutableCities.single { c -> c.id == randomCityId }
            val randomIndex = mutableCities.indexOf(randomCity)

            // NumberFormatException must allow to regenerate a new temperature
            var newTemp: Int
            do {
                newTemp = try {
                    temperatureRepository.fetchRandomValue()
                } catch (e: NumberFormatException) {
                    randomCity.temperature
                }
            }while (newTemp == randomCity.temperature)

            mutableCities[randomIndex] = randomCity.copy(temperature = newTemp)
            emit(mutableCities.sortedByTemperature())
        }
    }

    val state: LiveData<State> = liveData {
        cities
            .flowOn(Dispatchers.Default)
            .retry(RETRIES)
            .onStart { emit(State.Loading) }
            .catch { e ->
                e.printStackTrace()
                emit(State.Failure(e))
            }
            .collect { cities ->
                emit(State.Success(cities))
            }
    }

    sealed class State {
        object Loading : State()
        class Success(val cities: List<City>) : State()
        class Failure(val error: Throwable) : State()
    }

}