package com.dgianessi.surfing

import com.dgianessi.surfing.repository.CityRepository
import kotlinx.coroutines.delay

object CityRepositoryFake {

    fun success(names: List<String>? = null, initalDelay: Long = 0L) = object : CityRepository {
        override suspend fun fetchNames(): List<String> {
            delay(initalDelay)
            return names ?: listOf("fake", "fake2", "fake3")
        }
    }

    fun failure(initalDelay: Long = 0L) = object : CityRepository {
        override suspend fun fetchNames(): List<String> {
            delay(initalDelay)
            throw RuntimeException("Fake ex")
        }
    }

}