package com.dgianessi.surfing

import com.dgianessi.surfing.repository.TemperatureRepository

object TemperatureRepositoryFake {

    private val tempRange = 0..1000

    fun success() = object : TemperatureRepository {
        override suspend fun fetchRandomValue() = tempRange.random()
    }

    fun failure() = object : TemperatureRepository {
        override suspend fun fetchRandomValue() = throw RuntimeException("Fake ex")
    }

}