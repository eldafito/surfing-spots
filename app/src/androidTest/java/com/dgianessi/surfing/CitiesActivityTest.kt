package com.dgianessi.surfing

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.dgianessi.surfing.ui.CitiesActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.IsNot.not
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest

private const val INITIAL_DELAY = 500L
private const val NEW_STATE_DELAY = 1000L

@RunWith(AndroidJUnit4::class)
@LargeTest
class CitiesActivityTest : KoinTest {

    private val successModule = module {
        single(override = true) { CityRepositoryFake.success(initalDelay = INITIAL_DELAY) }
        single(override = true) { TemperatureRepositoryFake.success() }
    }

    private val failureModule = module {
        single(override = true) { CityRepositoryFake.failure(initalDelay = INITIAL_DELAY) }
        single(override = true) { TemperatureRepositoryFake.failure() }
    }

    @Test
    fun successfulLoading(): Unit = runBlocking {
        loadKoinModules(successModule)
        launchActivity<CitiesActivity>().use {
            onView(withId(R.id.cities_progress)).check(matches(isDisplayed()))
            onView(withId(R.id.cities_error_msg)).check(matches(not(isDisplayed())))

            delay(NEW_STATE_DELAY)

            onView(withId(R.id.cities_progress)).check(matches(not(isDisplayed())))
            onView(withId(R.id.cities_error_msg)).check(matches(not(isDisplayed())))
        }
    }

    @Test
    fun failureLoading(): Unit = runBlocking {
        loadKoinModules(failureModule)
        launchActivity<CitiesActivity>().use {
            onView(withId(R.id.cities_progress)).check(matches(isDisplayed()))
            onView(withId(R.id.cities_error_msg)).check(matches(not(isDisplayed())))

            delay(NEW_STATE_DELAY)

            onView(withId(R.id.cities_progress)).check(matches(not(isDisplayed())))
            onView(withId(R.id.cities_error_msg)).check(matches(isDisplayed()))
        }
    }

}