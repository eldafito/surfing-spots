package com.dgianessi.surfing.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dgianessi.surfing.network.MockyApi
import com.dgianessi.surfing.network.NumbersApi
import com.dgianessi.surfing.shared.appModule
import com.dgianessi.surfing.viewmodel.CitiesViewModel.State
import com.dgianessi.surfing.viewmodel.CitiesViewModel.State.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

private const val TIMEOUT = 10_000L
private const val UPDATE_TEMP_DELAY = 3000L
// first update is at time 0
private const val EXPECTED_UPDATE_TIMES = TIMEOUT.div(UPDATE_TEMP_DELAY).toInt().plus(1)

class CitiesViewModelTest : KoinTest {

    private val fakeNames = listOf("Cesena", "Milano", "San Jose")

    @Mock
    lateinit var observer: Observer<State>

    @Captor
    private lateinit var captor: ArgumentCaptor<State>

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val numbersMockServer = MockWebServer()
    private val mockyMockServer = MockWebServer()

    private val viewModel: CitiesViewModel by inject()

    private lateinit var mockRef: AutoCloseable

    private val testModule = module {
        single(override = true) { NumbersApi.create(numbersMockServer.url("/")) }
        single(override = true) { MockyApi.create(mockyMockServer.url("/")) }
    }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(appModule, testModule)
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        mockRef = MockitoAnnotations.openMocks(this)
        numbersMockServer.start(8607)
        mockyMockServer.start(8080)
        viewModel.state.observeForever(observer)
    }

    @Test
    fun `update number`() = runBlocking {
        numbersMockServer.dispatcher = NumbersDispatcher.success()
        mockyMockServer.dispatcher = MockyDispatcher.success(fakeNames)
        inOrder(observer).run {
            verify(observer).onChanged(Loading)
            verify(
                observer,
                timeout(TIMEOUT).times(EXPECTED_UPDATE_TIMES)
            ).onChanged(isA(Success::class.java))
        }
    }

    @Test
    fun `city names`() = runBlocking {
        numbersMockServer.dispatcher = NumbersDispatcher.success()
        mockyMockServer.dispatcher = MockyDispatcher.success(fakeNames)
        inOrder(observer).run {
            verify(observer).onChanged(Loading)
            verify(
                observer,
                timeout(TIMEOUT).times(EXPECTED_UPDATE_TIMES)
            ).onChanged(captor.capture())
        }
        for (s in captor.allValues) {
            assert(s is Success)
            val cities = (s as Success).cities
            assert(cities.map { c -> c.name }.containsAll(fakeNames))
        }
    }

    @Test
    fun `temperature sorting`() = runBlocking {
        numbersMockServer.dispatcher = NumbersDispatcher.success()
        mockyMockServer.dispatcher = MockyDispatcher.success(fakeNames)
        inOrder(observer).run {
            verify(observer).onChanged(Loading)
            verify(
                observer,
                timeout(TIMEOUT).times(EXPECTED_UPDATE_TIMES)
            ).onChanged(captor.capture())
        }
        for (s in captor.allValues) {
            assert(s is Success)
            val cities = (s as Success).cities
            val tempSortedCities = cities.sortedByDescending { c -> c.temperature }
            assert(cities == tempSortedCities)
        }
    }

    @Test
    fun `random equity`() = runBlocking {
        numbersMockServer.dispatcher = NumbersDispatcher.success()
        mockyMockServer.dispatcher = MockyDispatcher.success(fakeNames)
        inOrder(observer).run {
            verify(observer).onChanged(Loading)
            // in first success state random round hasn't started yet
            verify(
                observer,
                timeout(TIMEOUT).times(fakeNames.size + 1)
            ).onChanged(captor.capture())
        }
        assert(captor.allValues.all { s -> s is Success })

        val firstRound =
            (captor.allValues.first() as Success).cities.sortedBy { it.id }.map { it.temperature }
        val lastRound = (captor.value as Success).cities.sortedBy { it.id }.map { it.temperature }

        // every temperature must be changed in the same round
        firstRound.zip(lastRound).forEach { comp ->
            assert(comp.first != comp.second)
        }
    }

    @Test
    fun `city failure`() = runBlocking {
        numbersMockServer.dispatcher = NumbersDispatcher.success()
        mockyMockServer.dispatcher = MockyDispatcher.failure()
        inOrder(observer).run {
            verify(observer).onChanged(Loading)
            verify(observer, timeout(TIMEOUT)).onChanged(isA(Failure::class.java))
            verifyNoMoreInteractions()
        }
    }

    @Test
    fun `temperature failure`() = runBlocking {
        numbersMockServer.dispatcher = NumbersDispatcher.failure()
        mockyMockServer.dispatcher = MockyDispatcher.success(fakeNames)
        inOrder(observer).run {
            verify(observer).onChanged(Loading)
            verify(observer, timeout(TIMEOUT)).onChanged(isA(Failure::class.java))
            verifyNoMoreInteractions()
        }
    }

    @After
    fun tearDown() {
        numbersMockServer.shutdown()
        mockyMockServer.shutdown()
        mockRef.close()
    }

}