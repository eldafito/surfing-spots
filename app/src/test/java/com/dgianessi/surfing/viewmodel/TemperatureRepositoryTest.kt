package com.dgianessi.surfing.viewmodel

import com.dgianessi.surfing.network.NumbersApi
import com.dgianessi.surfing.repository.TemperatureRepository
import com.dgianessi.surfing.repository.TemperatureRepositoryImpl
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException

class TemperatureRepositoryTest {

    private val mockServer = MockWebServer()
    private lateinit var temperatureRepository: TemperatureRepository

    @Before
    fun setUp() {
        mockServer.start()
        temperatureRepository = TemperatureRepositoryImpl(NumbersApi.create(mockServer.url("/")))
    }

    @Test
    fun success() = runBlocking {
        mockServer.dispatcher = NumbersDispatcher.success("3 magic number")
        val temp = temperatureRepository.fetchRandomValue()
        assert(temp == 3)
    }

    @Test(expected = HttpException::class)
    fun failure(): Unit = runBlocking {
        mockServer.dispatcher = NumbersDispatcher.failure()
        temperatureRepository.fetchRandomValue()
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

}