package com.dgianessi.surfing.viewmodel

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

object NumbersDispatcher {

    private val randomRange = 0..1000

    /**
     * Returns a successful response with given [body] or a string starting with a random number.
     */
    fun success(body: String? = null) = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(body ?: "${randomRange.random()} is a fake number")
                .throttleBody(1024, 2, TimeUnit.SECONDS)
        }
    }

    /**
     * Returns a NOT FOUND response.
     */
    fun failure() = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_NOT_FOUND)
                .throttleBody(1024, 2, TimeUnit.SECONDS)
        }
    }

}