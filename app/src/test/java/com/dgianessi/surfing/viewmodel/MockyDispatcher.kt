package com.dgianessi.surfing.viewmodel

import com.dgianessi.surfing.network.CitiesRemote
import com.google.gson.Gson
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

object MockyDispatcher {

    /**
     * Returns a successful response using resources json file or [names].
     */
    fun success(names: List<String>? = null) = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            val jsonBody = names?.let { ns ->
                val cities = CitiesRemote(ns.map { n -> CitiesRemote.CityRemote(n) })
                Gson().toJson(cities)
            } ?: this.javaClass.classLoader!!.getResource("cities.json").readText()

            return MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(jsonBody)
                .throttleBody(1024, 500, TimeUnit.MILLISECONDS)
        }
    }

    /**
     * Returns a NOT FOUND response.
     */
    fun failure() = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_NOT_FOUND)
                .throttleBody(1024, 2, TimeUnit.SECONDS)
        }
    }

}