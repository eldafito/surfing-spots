package com.dgianessi.surfing.viewmodel

import com.dgianessi.surfing.network.MockyApi
import com.dgianessi.surfing.repository.CityRepository
import com.dgianessi.surfing.repository.CityRepositoryImpl
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException


class CityRepositoryTest {

    private val fakeNames = listOf("Cesena", "Milano", "San Jose")

    private val mockServer = MockWebServer()
    private lateinit var cityRepository: CityRepository

    @Before
    fun setUp() {
        mockServer.start()
        cityRepository = CityRepositoryImpl(MockyApi.create(mockServer.url("/")))
    }

    @Test
    fun success(): Unit = runBlocking {
        mockServer.dispatcher = MockyDispatcher.success(fakeNames)
        val names = cityRepository.fetchNames()
        assert(names == fakeNames)
    }

    @Test(expected = HttpException::class)
    fun failure(): Unit = runBlocking {
        mockServer.dispatcher = MockyDispatcher.failure()
        cityRepository.fetchNames()
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

}